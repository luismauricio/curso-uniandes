const assert = require('assert')
const ganache = require('ganache-cli')
const Web3 = require('web3')
const web3 = new Web3(ganache.provider())
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const compiledFactory = require('../build/CrowdfundingFactory.json')
const compiledCrowdfunding = require('../build/Crowdfunding.json')

let accounts
let campaign
let contractFactory

beforeEach(async () => {
  accounts = await web3.eth.getAccounts()
  
  let factoryBytecode = compiledFactory.evm.bytecode.object
  let factoryInterface = JSON.parse(compiledFactory.metadata).output.abi

  contractFactory = await new web3.eth.Contract(factoryInterface)
                          .deploy({ data: factoryBytecode })
                          .send({ from: accounts[0], gas: '2100000' })
  
  await contractFactory.methods.createCampaign('1000').send({
    from: accounts[0],
    gas: '2100000'
  })

  let deployedCampaigns = await contractFactory.methods.getDeployedCampaigns().call()
  campaignAddress = deployedCampaigns[0]

  let crowdfundingInterface = JSON.parse(compiledCrowdfunding.metadata).output.abi
  campaign = await new web3.eth.Contract(crowdfundingInterface, campaignAddress)
})

describe('Campaigns', () => {
  it('Check if contracts are deployed', async () => {
    assert.ok(contractFactory.options.address)
    assert.ok(campaign.options.address)
  }) 

  it('Processes request',async () =>{
    let initialContributorBalance=await web3.eth.getBalance(accounts[1])
    initialContributorBalance= web3.utils.fromWei(initialContributorBalance,'ether')
    initialContributorBalance=parseFloat(initialContributorBalance)
    
    let initialProviderBalance = await web3.eth.getBalance(accounts[2])
    initialProviderBalance= parseFloat(initialProviderBalance)
    
    
    await campaign.methods.contribute().send({
    from : accounts[1],
    value : web3.utils.toWei('5','ether')
    })
    
    await campaign.methods.createRequest('comprar baterias',web3.utils.toWei('3','ether'),accounts[2])
    .send({from: accounts[0],gas: '2100000'})
    
    await campaign.methods.approveRequest(0)
    .send({from: accounts[1],gas: '2100000'})
    await campaign.methods.finalizeRequest(0)
    .send({from: accounts[0],gas: '2100000'})
    
    let providerBalance = await web3.eth.getBalance(accounts[2])
    providerBalance=parseFloat(providerBalance)
    console.log(providerBalance)
    
    let contributorBalance = await web3.eth.getBalance(accounts[1])
    contributorBalance=parseFloat(contributorBalance)
    console.log(contributorBalance)
    
    let requestAmount = web3.utils.toWei('3','ether')
    requestAmount =parseFloat(requestAmount)
    assert.equal(providerBalance,initialProviderBalance+requestAmount)
    
    }) 
})
