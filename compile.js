const path = require('path')
const solc = require('solc')
const fs = require('fs-extra')

const buildPath = path.resolve(__dirname, 'build')
fs.removeSync(buildPath)

const contractFactoryPath = path.resolve(__dirname, 'contracts', 'CrowdfundingFactory.sol')
const contractFactorySource = fs.readFileSync(contractFactoryPath, 'utf8')

const contractCrowdfundingPath = path.resolve(__dirname, 'contracts', 'Crowdfunding.sol')
const contractCrowdfundingSource = fs.readFileSync(contractCrowdfundingPath, 'utf8')

const contractManagedPath = path.resolve(__dirname, 'contracts', 'Managed.sol')
const contractManagedSource = fs.readFileSync(contractManagedPath, 'utf8')

const crowdfundingContractsInput = {
  language: 'Solidity',
  sources: {
    'CrowdfundingFactory.sol': {
      content: contractFactorySource
    },
    'Crowdfunding.sol': {
      content: contractCrowdfundingSource
    },
    'Managed.sol': {
      content: contractManagedSource
    }
  },
  settings: {
    outputSelection: {
      '*': {
        '*': [ "metadata", "evm.bytecode" ]
      }
    }
  }
}

const crowdfundingOutput = JSON.parse(solc.compile(JSON.stringify(crowdfundingContractsInput))).contracts
fs.ensureDirSync(buildPath)

for (let contract in crowdfundingOutput) {
  fs.outputJsonSync(
    path.resolve(buildPath, `${contract.replace('.sol', '')}.json`),
    crowdfundingOutput[contract][contract.replace('.sol', '')]
  )
}
