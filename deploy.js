const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')
const compiledFactory = require('./build/CrowdfundingFactory.json')

const provider = new HDWalletProvider(
  'giggle inflict accident close cushion coconut okay obtain history cheap mansion model', 
  'https://rinkeby.infura.io/v3/85ad98fea8f240a1aeb38230d304d419'
)

const web3 = new Web3(provider)

const deploy = async () => {
  const accounts = await web3.eth.getAccounts()
  console.log('Cuanta que usaremos para crear nuestro contrato', accounts[0])

  let factoryBytecode = compiledFactory.evm.bytecode.object
  let factoryInterface = JSON.parse(compiledFactory.metadata).output.abi

  let contractFactory = await new web3.eth.Contract(factoryInterface)
                              .deploy({ data: `0x${factoryBytecode}` })
                              .send({ from: accounts[0], gas: '2100000' })
  
  console.log('Contrato desplegado en', contractFactory.options.address)                      
}

deploy()
