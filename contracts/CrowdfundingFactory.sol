pragma solidity >=0.5.10 <0.7.0;

import './Crowdfunding.sol';

contract CrowdfundingFactory {
	address[] public deployedCampaigns;
	event LogNewCampaign(address newCampaignAddress, address campaignManager, uint minimumContribution);
	
	function createCampaign (uint _minimumContribution) public returns (address _newCampaign) {
		Crowdfunding newCampaign = new Crowdfunding(_minimumContribution, msg.sender);
		deployedCampaigns.push(address(newCampaign));
		emit LogNewCampaign(address(newCampaign), msg.sender, _minimumContribution);
		return address(newCampaign);
	}
	
	function getDeployedCampaigns() public view returns (address[] memory _deployedCampaigns) {
		return deployedCampaigns;
	}
}
