pragma solidity >=0.5.10 <0.7.0;

import './Managed.sol';

contract Crowdfunding is Managed {
	struct Request {
		string description;
		uint amount;
		address payable recipient;
		bool complete;
		uint approvalCount;
		mapping(address => bool) approvals;
	}
	
	uint public minimumContribution;
	mapping(address => bool) public contributors;
	Request[] public requests;
	uint public contributorsCount;
	
	event LogNewContribution(address contributorAddress, address crowdfundingAddress, uint contributionAmount);
	
	constructor (uint _minimumContribution, address _initialManager) public Managed(_initialManager) {
		minimumContribution = _minimumContribution;
	}
	
	function contribute() public payable {
		require(msg.value >= minimumContribution, 'Tiene que buscar más dinero');
		contributors[msg.sender] = true;
		contributorsCount++;
		emit LogNewContribution(msg.sender, address(this), msg.value);
	}
	
	function createRequest (string memory _description, uint _amount, address payable _recipient) 
		public
		onlyManager
	{
		Request memory newRequest = Request({
			description: _description,
			amount: _amount,
			recipient: _recipient,
			complete: false,
			approvalCount: 0
		});
			
		requests.push(newRequest);
	}
	
	function approveRequest(uint _index) public {
		Request storage request = requests[_index];
		
		require(contributors[msg.sender], 'Ud no es contribuyente');
		require(!request.approvals[msg.sender], 'Ud ya votó');
		
		request.approvals[msg.sender] = true;
		request.approvalCount++;
	}
	
	function finalizeRequest(uint _index) public onlyManager {
		Request storage request = requests[_index];
		
		require(!request.complete);
		require(request.approvalCount > (contributorsCount / 2));
		
		request.recipient.transfer(request.amount);
		request.complete = true;
	}
	
	function getCrowdfundingBalance() public view returns (uint _currentCrowdfundingBalance) {
		return address(this).balance;
	}
}
