pragma solidity >=0.5.10 <0.7.0;

contract Managed {
	address public manager;

	modifier onlyManager {
		require(msg.sender == manager);
		_;
	}

	constructor(address _initialManager) public {
		manager = _initialManager;
	}

	function getManager() public view returns(address _currentManager) {
		return manager;
	}

	function setNewManager(address _newManager) public {
		manager = _newManager;
	}
}

